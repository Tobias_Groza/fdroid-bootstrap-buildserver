# F-Droid bootstrap buildserver

**WARNING**: Project is still under development, so things might change a lot.
This is also not suited for doing a production setup, use with care.

This is a script for bootstapping
[fdroidserver](https://gitlab.com/fdroid/fdroidserver) with a nested
libvirt/KVM buildserver setup on Debian 9 (stretch).

Running this ansible playbook will do the following things:
* create `fdroid` user
* update/install dependencies for running F-Droid buildserver
* update/install fdroidserver and fdroiddata to latest master
* start our `makebuildserver` script in a screen session

## System requirements

* Debian 9 (stretch)
* amd64 based CPU with VMX support (for now only intel)
* min. 200GB disk space (SSD recommended)
* VM will require 4GB of RAM (5GB when using vagrant)

(Running the setup will donwload about 15GB on the first run. So a reasonably fast internet connection is required too.)

## Deployment to remote server

You simply need to run the playbook from this project to bootstrap
fdroid server to a remote host.

First you'll need to install ansible, on your local machine e.g.:

    sudo apt install ansible

You'll also need to make sure that the bare minimum for ansible
is installed on your remote host:

    sudo apt install python3 sudo

Fetch ansible dependencies:

    ansible-galaxy install -f -r requirements.yml -p .galaxy

Then you can immediately start the deployment:

    ansible-playbook -i remoteHost, provision.yml

You'll need to replace `remoteHost` with the name of the remote host as
configured in your ssh config. (Tip: See `man 5 ssh_config` for help with
ssh config.) You'll also need to be root or have sudo privileges for
successfully running this script.

You can follow the progress of `makebuildserver` by running:

    ssh remoteHost tail -f /home/fdroid/makebuildserver.log

This might take a couple of hours to finish thou.
Once it finished successfully, you can go ahead and start building apps:

    ssh remoteHost
    sudo -i -u fdroid
    cd ~/fdroiddata
    fdroid build --server --latest org.fdroid.fdroid

(The first time you build, you may need to add `--reset-server` to the
`fdroid build` line above in case you see an error like in #8)

## Deployment to local vagrant VM

Using this project also automates bootstraping a (double nested) installation
of fdroidserver (+ buildserver VM) to a local vagrant VM.

First of all you'll need to make sure you have vagrant, libvirt, kvm
and ansible installed.

    sudo apt install ansible libvirt-daemon-system vagrant-libvirt

Give your user permission to work with libvirt/KVM VMs.

    sudo adduser $USER libvirt
    sudo adduser $USER libvirt-qemu

Clone this project

    git clone https://gitlab.com/uniqx/fdroid-bootstrap-buildserver
    cd fdroid-bootstrap-buildserver

Make sure KVM nesting is installed. We supply a convenience script for doing so:

    sudo bash enable_kvm_nesting.sh

Use vagrants normal workflow to get started. (It's recommended to run over
night, since this will take several hours to complete and hog system
resources.)

    vagrant up

NOTE: Should the provisioning process get interruped or fail for occult reasons you
can contine by running `vagrant provision`. (eg. libvirt will suspend the VM
should you run out of disk space.)

When vagrant provisioning finished successfully, `makebuildserver` is still
running in a screen-session in the newly created vagrant container. You can
use this command to follow the progress:

    vagrant ssh -c 'tail -f /home/fdroid/makebuildserver.log'

Once `makebuildserver` finishes successfully you can connect to the VM
and start building apps:

    vagrant ssh
    sudo -i -u fdroid
    cd ~/fdroiddata
    fdroid build --server --latest org.fdroid.fdroid


## Deploy to local system

Warning: Don't use this method! You might mess up your system. Use the
vagrant method if you just need a local buildserver.

In some rare situations it makes sense to just bootstrap a buildserver on
your local machine. Like when setting up a dedicated system where sshd is not
installled or impractical.

    ansible-galaxy install -f -r requirements.yml -p .galaxy
    ansible-playbook --ask-become-pass -c local provision.yml
